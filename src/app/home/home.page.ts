import { Component } from '@angular/core';

import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';

let { Camera } = Plugins;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  cameraResult = '';
  photoDataUrl;

  constructor() {}

  async runCamera () {
    this.cameraResult = '';
    try {
      let photo = await Camera.getPhoto({
        resultType: CameraResultType.DataUrl,
        source: CameraSource.Photos,
      });
      this.cameraResult = 'Resolve';
      this.photoDataUrl = photo.dataUrl;
    } catch (error) {
      this.cameraResult = 'Reject'; // ! can not reach this line if swipe down UIImagePicker
    }
  }

}
